﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Inimene
    {
        public string Nimi;
        public DateTime Sünniaeg;

        public Inimene PaneUusNimi(string nimi)
        {
            Nimi = nimi.Substring(0, 1).ToUpper() + nimi.Substring(1).ToLower();
            return this;
        }

        public Inimene UusSünniaeg(DateTime d)
        {
            //if (d > DateTime.Now) Sünniaeg = DateTime.Now.Date;
            //else Sünniaeg = d;

            Sünniaeg = d > DateTime.Now ? DateTime.Now.Date : d;
            return this;
        }

        public int AnnaVanus()
        {
            int vanus = DateTime.Now.Year - Sünniaeg.Year;
            if (Sünniaeg.AddYears(vanus) > DateTime.Now)
                vanus--;
            return vanus;

        }

        public Inimene Tryki0()
        {
            Console.WriteLine(this);
            return this;
        }

        public static Inimene Tryki1(Inimene i)
        {
            Console.WriteLine(i);
            return i;
        }

        public override string ToString()
        {
            //int vanus = DateTime.Now.Year - Sünniaeg.Year;
            //// kaks varianti sama asja üleskirjutamiseks

            //// 1. avaldisega
            //// vanus -= (Sünniaeg.AddYears(vanus) > DateTime.Now ? 1 : 0);

            //// 2. if lausega
            //if (Sünniaeg.AddYears(vanus) > DateTime.Now)
            //    vanus--;
            
            return $"{Nimi} - {AnnaVanus()} aastat vana" ;
        }
    }
    class Program
    {

        static int Liida(int a, int b)
        {
            return a + b;
        }

        static int Liida(int a, int b, int c)
        {
            return a + Liida(b, c);
        }
        static void Main()
        {
            // testime

            Console.WriteLine(Liida(4,7));


            Inimene test = new Inimene {Nimi= "nimetu" };
            Console.WriteLine(test);
            test
                .PaneUusNimi("nimega")
                .UusSünniaeg(DateTime.Parse("2000/01/01"))
                //.Tryki()
                .PaneUusNimi("veeluuemnimi")
                .Tryki0()
                
                ;
            
            // teeme listi inimestest
            List<Inimene> nimekiri = new List<Inimene>
            {
                new Inimene {Nimi = "Henn", Sünniaeg = DateTime.Parse("1955/03/07")},
                new Inimene {Nimi = "Ants", Sünniaeg = DateTime.Parse("1980/06/22")},
                new Inimene {Nimi = "Peeter", Sünniaeg = DateTime.Parse("1977/07/01")},
                new Inimene {Nimi = "Toomas", Sünniaeg = DateTime.Parse("1992/08/07")},
                new Inimene {Nimi = "Malle", Sünniaeg = DateTime.Parse("1995/10/12")},
                new Inimene {Nimi = "Anu", Sünniaeg = DateTime.Parse("1982/09/13")},
                new Inimene {Nimi = "Kalle", Sünniaeg = DateTime.Parse("2000/02/29")},
                new Inimene {Nimi = "Tiit", Sünniaeg = DateTime.Parse("1999/09/13")},
                new Inimene {Nimi = "Sille", Sünniaeg = DateTime.Parse("1948/08/26")},
                new Inimene {Nimi = "Pille", Sünniaeg = DateTime.Parse("1960/02/28")},
            };

            if (nimekiri[0].AnnaVanus() > nimekiri[1].AnnaVanus())
                Console.WriteLine("esimene on vanem");

            double keskmine = 0;
            foreach (var x in nimekiri)
                keskmine += x.AnnaVanus();
            Console.WriteLine($"keskmine vanus on {keskmine / nimekiri.Count}");

            foreach (var x in nimekiri) Inimene.Tryki1(x);
            foreach (var x in nimekiri) x.Tryki0();

            // nüüd oleks vaja leida kellel on järgmine sünnipäev
            int järgmiseni = 400;
            DateTime sünnipäev = DateTime.Now;
            string kellel = "";
            foreach(var x in nimekiri)
            {
                int v = DateTime.Now.Year - x.Sünniaeg.Year;
                DateTime d = x.Sünniaeg.AddYears(v);
                if (d < DateTime.Now) d = d.AddYears(1);
                if ((d - DateTime.Now).Days < järgmiseni) 
                {
                    järgmiseni = (d - DateTime.Now).Days;
                    kellel = x.Nimi;
                    sünnipäev = d;
                }
                
            }
            Console.WriteLine("järgmise sünnipäevani on {0} päeva ", järgmiseni);
            Console.WriteLine($"siis {sünnipäev} on {kellel} sünnipäev");

            Console.WriteLine(DateTime.Now.Ticks);



        }
    }
}
